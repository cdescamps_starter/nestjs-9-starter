import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as basicAuth from 'express-basic-auth';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // Prefixe toutes les routes par /api
  app.setGlobalPrefix('/api');

  // Middleware pour le Swagger
  app.use(['/api', '/api-json'], basicAuth({
    challenge: true,
    users: {
      [process.env.SWAGGER_USER]: process.env.SWAGGER_PASSWORD,
    },
  }));

  // Déploiement du Swagger sur la route /api
  const config = new DocumentBuilder()
    .setTitle('unoWeb API')
    .setDescription("Documentation de l'API unoWeb")
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  // Démarage du serveur
  if(process.env.PORT) {
    await app.listen(process.env.PORT);
    console.log("Server started on port : "+process.env.PORT)
  }
  else {
    console.log("Aucun port spécifié ( vérifier .env )")
  }
}
bootstrap();
