import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
require('dotenv').config();

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'mysql',
    extra : {
      "socketPath": "/var/run/mysqld/mysqld.sock"
    },
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    entities: [],
    synchronize: false,
  }),ConfigModule.forRoot(), UsersModule,],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
